﻿#include <iostream>
#include <string>
#include <time.h>
#include <math.h>
#include "Helpers.h"

void PrintOddNumbers(int limit, bool isOdd)
{
    for (int i = isOdd; i < limit; i += 2)
    {
        std::cout << i << '\n';
    }
}

class Vector
{
public:
    Vector() 
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    double GetX()
    {
        return x;
    }

    double GetY()
    {
        return y;
    }

    double GetZ()
    {
        return z;
    }

    double length()
    {
        return std::sqrt(x * x + y * y + z * z);
    }

private:
    double x = 0;
    double y = 0;
    double z = 0;
};

class Character
{
public:
    std::string GetNickname()
    {
        return nickname;
    }

    void SetNickname(std::string value)
    {
        nickname = value;
    }

    int GetScore()
    {
        return score;
    }

    void SetScore(int value)
    {
        score = value;
    }
private:
    std::string nickname;
    int score;
};

void selectionsort(Character* list, int l, int r) {
    for (int i = l; i < r; i++) {
        int minz = list[i].GetScore(), ind = i;
        for (int j = i + 1; j < r; j++) {
            if (minz < list[j].GetScore()) minz = list[j].GetScore(), ind = j;
        }
        std::swap(list[i], list[ind]);
    }
}

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Base" << '\n';
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};

class Donkey : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Hee-haw!\n";
    }
};

int main()
{
    Animal *animals[] = {new Dog(), new Cat(), new Cow(), new Donkey()};

    int count = sizeof(animals) / sizeof(Animal);

    for (Animal** ptr = animals; ptr < animals + count; ptr++)
    {
        (*ptr)->Voice();
        delete (*ptr);
    }    
    
}

