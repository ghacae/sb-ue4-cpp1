#pragma once

int paw(int a, int b)
{
	return (a + b) * (a + b);
}

int paw(float a, float b)
{
	return (a + b) * (a + b);
}

int paw(double a, double b)
{
	return (a + b) * (a + b);
}